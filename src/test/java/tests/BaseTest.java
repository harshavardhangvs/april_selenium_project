package tests;

import driver.DriverFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import reports.ReportManager;

public class BaseTest {


    @BeforeMethod
    public void initBrowser() {
        DriverFactory.initDriver();
    }

    @AfterMethod
    public void quitBrowser() {
        DriverFactory.quitDriver();
        ReportManager.removeExtentTest();
    }
}