package tests;

import annotations.ExtentReportInfo;
import enums.ConfigEnums;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.OrangeDashBoardPage;
import pages.OrangeLoginPage;
import utils.ConfigUtil;
import utils.DecodeUtil;

import java.util.Map;

public class OrangeLoginPageTest extends BaseTest {
    OrangeLoginPage orangeLoginPage;

    @ExtentReportInfo(authors = {"harsha", "vardhan"}, devices = {"windows", "mac"})
    @Test(groups = {"smoke", "sanity"})
    public void testLandingPage() {
        orangeLoginPage = new OrangeLoginPage();
        Assert.assertEquals(orangeLoginPage.getPageTitle(), "OrangeHRM", "Did not landed on login page!!");
    }

    @ExtentReportInfo(authors = {"goutham"}, devices = {"linux", "mac"})
    @Test(groups = {"regression"})
    public void testVisibilityOfForgetPasswordLinkOnLoginPage() {
        Assert.assertTrue(new OrangeLoginPage().isForgotPasswordDisplayed(), "forget password link is not found!!");
    }

    @ExtentReportInfo(authors = {"pushpa raj"}, devices = {"windows", "linux"})
    @Test
    public void testLogin() {
        System.out.println("in test login");
        orangeLoginPage = new OrangeLoginPage();
        String password = DecodeUtil.getDecodeString(ConfigUtil.getPropertyValue(ConfigEnums.PASSWORD));
        String headerName = orangeLoginPage.enterUserName(ConfigUtil.getPropertyValue(ConfigEnums.USERNAME))
                .enterPassword(password)
                .clickLoginButton()
                .getHeaderName();
        Assert.assertEquals(headerName, "Dashboard123",
                "Did not landed on dashboard page!!");
    }

    //    @Test(dataProvider = "loginDataProvider", dataProviderClass = DataProvider.class)
    public void testLogin2(Map<String, String> cred) {
        orangeLoginPage = new OrangeLoginPage();
        String password = cred.get(ConfigEnums.PASSWORD.toString().toLowerCase());
        String username = cred.get(ConfigEnums.USERNAME.toString().toLowerCase());
        OrangeDashBoardPage orangeDashBoardPage = orangeLoginPage.enterUserName(username)
                .enterPassword(password)
                .clickLoginButton();
        if (username.equals("Admin") && password.equals("admin123")) {
            String headerName = orangeDashBoardPage.getHeaderName();
            Assert.assertEquals(headerName, "Dashboard",
                    "Did not landed on dashboard page!!");
        } else {
            Assert.assertTrue(orangeLoginPage.isInvalidPasswordMessageDisplayed(),
                    "invalid error message is not displayed for invalid credentials!!");
        }
    }
}