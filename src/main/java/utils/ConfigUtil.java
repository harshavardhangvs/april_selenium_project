package utils;

import FrameworkConstants.Constants;
import enums.ConfigEnums;
import exceptions.ImpoperConfigFileValueException;
import exceptions.ImproperConfigFileKeyException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public class ConfigUtil {
    private static Properties prop;

    static {
        try {
            FileInputStream fis = new FileInputStream(Constants.getConfigFilePath());
            prop = new Properties();
            prop.load(fis);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getPropertyValue(ConfigEnums url) {
        String key = url.toString().toLowerCase();
        if (key.isBlank() || key.isEmpty() || Objects.isNull(key)) {
            throw new ImproperConfigFileKeyException("Key is either empty or null!!");
        }
        String value = prop.getProperty(key).trim();
        if (value.isEmpty() || value.isBlank() || Objects.isNull(value)) {
            throw new ImpoperConfigFileValueException(String.format("Property value of key %s is either null or blank!!", key));
        } else {
            return value;
        }
    }
}