package utils;

import FrameworkConstants.Constants;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class ExcelUtil {

    private ExcelUtil() {
    }

    public static List<Map<String, String>> getTestCredentials() {
        List<Map<String, String>> data = new ArrayList<>();
        try (FileInputStream fis = new FileInputStream(Constants.getExcelFilePath())) {
            XSSFWorkbook workbook = new XSSFWorkbook(fis);
            XSSFSheet sheet = workbook.getSheet("Sheet1");
            int rowcount = sheet.getLastRowNum() + 1;
            int colcount = sheet.getRow(0).getLastCellNum();
            Map<String, String> map = null;
            for (int i = 1; i < rowcount; i++) {
                map = new HashMap<>();
                for (int j = 0; j < colcount; j++) {
                    DataFormatter dataFormatter = new DataFormatter();
                    XSSFCell keycell = sheet.getRow(0).getCell(j);
                    XSSFCell valuecell = sheet.getRow(i).getCell(j);
                    map.put(dataFormatter.formatCellValue(keycell), dataFormatter.formatCellValue(valuecell));
                }
                data.add(map);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
}