package utils;

import enums.ConfigEnums;

public final class SystemPropertyUtil {

    private SystemPropertyUtil() {
    }

    public static String getBrowserName() {
        return System.getProperty(ConfigEnums.BROWSER.toString().toLowerCase());
    }
}
