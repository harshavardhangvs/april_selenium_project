package utils;

import java.util.Base64;

public final class DecodeUtil {
    private DecodeUtil() {
    }

    public static String getDecodeString(String base64EncodedString) {
        return new String(Base64.getDecoder().decode(base64EncodedString));
    }
}