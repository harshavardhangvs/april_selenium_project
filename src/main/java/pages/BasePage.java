package pages;

import driver.DriverFactory;
import org.openqa.selenium.By;
import reports.ReportFactory;

public class BasePage {

    protected void sendKeys(By by, String data, String elementName) {
        DriverFactory.getDriver().findElement(by).sendKeys(data);
        ReportFactory.getExtentTest().info(String.format("enter text into %s: %s", elementName, data));
    }

    protected void click(By by, String elementName) {
        DriverFactory.getDriver().findElement(by).click();
        ReportFactory.getExtentTest().info(String.format("clicking %s", elementName));
    }

    protected boolean isDisplayed(By by, String elementName) {
        ReportFactory.getExtentTest().info(String.format("checking %s is displayed!", elementName));
        return DriverFactory.getDriver().findElement(by).isDisplayed();
    }

    public String getPageTitle() {
        String pageTitle = DriverFactory.getDriver().getTitle().trim();
        ReportFactory.getExtentTest().info(String.format("got page title as %s", pageTitle));
        return pageTitle;
    }

    public String getElementText(By by, String elementName) {
        String text = DriverFactory.getDriver().findElement(by).getText().trim();
        ReportFactory.getExtentTest().info(String.format("got element text as %s from %s element", text, elementName));
        return text;
    }
}