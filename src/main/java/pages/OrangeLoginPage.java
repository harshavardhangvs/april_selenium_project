package pages;

import org.openqa.selenium.By;

public class OrangeLoginPage extends BasePage {

    private final By USERNAME_LOCATOR = By.id("txtUsername");
    private final By PASSWORD_LOCATOR = By.id("txtPassword");
    private final By LOGIN_BTN_LOCATOR = By.id("btnLogin");
    private final By FORGOT_PASSWORD_LOCATOR = By.cssSelector("#forgotPasswordLink > a");
    private final By INVALID_CREDENTIALS_LOCATOR = By.cssSelector("span#spanMessage");

    public OrangeLoginPage enterUserName(String username) {
        sendKeys(USERNAME_LOCATOR, username,"username field");
        return this;
    }

    public OrangeLoginPage enterPassword(String password) {
        sendKeys(PASSWORD_LOCATOR, password,"password field");
        return this;
    }

    public OrangeDashBoardPage clickLoginButton() {
        click(LOGIN_BTN_LOCATOR,"login button");
        return new OrangeDashBoardPage();
    }

    public void clickForgotPassword() {
        click(FORGOT_PASSWORD_LOCATOR,"forgot password link");
    }

    public boolean isForgotPasswordDisplayed() {
        return isDisplayed(FORGOT_PASSWORD_LOCATOR,"forgot password link");
    }

    public boolean isInvalidPasswordMessageDisplayed() {
        return isDisplayed(INVALID_CREDENTIALS_LOCATOR,"Invalid Password Message");
    }
}