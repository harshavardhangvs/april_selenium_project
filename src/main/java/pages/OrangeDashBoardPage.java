package pages;

import org.openqa.selenium.By;

public class OrangeDashBoardPage extends BasePage {

    private final By HEADER_LOCATOR = By.xpath("//h1");

    public String getHeaderName() {
        return getElementText(HEADER_LOCATOR,"page header");
    }
}
