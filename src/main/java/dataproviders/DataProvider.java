package dataproviders;

import utils.ExcelUtil;

public final class DataProvider {

    private DataProvider() {
    }

    @org.testng.annotations.DataProvider
    public static Object[] loginDataProvider() {
        return ExcelUtil.getTestCredentials().toArray();
    }
}