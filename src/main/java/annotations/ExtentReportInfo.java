package annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ExtentReportInfo {

    String[] authors() default {};

    String[] devices() default {};

}
