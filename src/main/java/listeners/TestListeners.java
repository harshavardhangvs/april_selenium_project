package listeners;

import annotations.ExtentReportInfo;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.model.Media;
import driver.DriverFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.*;
import reports.ReportFactory;

public class TestListeners implements ITestListener, ISuiteListener {
    @Override
    public void onStart(ISuite suite) {
        ReportFactory.createExtentReport(suite.getName());
    }

    @Override
    public void onFinish(ISuite suite) {
        ReportFactory.flushExtentReport();
    }

    @Override
    public void onTestStart(ITestResult result) {
        String[] groups = result.getMethod().getGroups();

        String[] authors = result.getMethod()
                .getConstructorOrMethod()
                .getMethod().getAnnotation(ExtentReportInfo.class).authors();

        String[] devices = result.getMethod()
                .getConstructorOrMethod()
                .getMethod().getAnnotation(ExtentReportInfo.class).devices();

        ReportFactory.createExtentTest(result.getMethod().getMethodName(), groups, authors, devices);
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        ReportFactory.getExtentTest().pass(result.getMethod().getMethodName() + " passed!!");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        ReportFactory.getExtentTest().fail(result.getTestName() + " failed!!");
        String screenshot = ((TakesScreenshot) DriverFactory.getDriver()).getScreenshotAs(OutputType.BASE64);
        Media scshot = MediaEntityBuilder.createScreenCaptureFromBase64String(screenshot).build();
        ReportFactory.getExtentTest().fail(scshot);
    }

    @Override
    public void onTestSkipped(ITestResult result) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onTestFailedWithTimeout(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {


    }

    @Override
    public void onFinish(ITestContext context) {

    }
}
