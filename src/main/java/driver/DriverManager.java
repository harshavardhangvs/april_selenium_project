package driver;

import org.openqa.selenium.WebDriver;

public final class DriverManager {
    private DriverManager() {
    }

    private static ThreadLocal<WebDriver> tl = new ThreadLocal<>();

    public static void setDriver(WebDriver driver) {
        tl.set(driver);
    }

    public static WebDriver getDriver() {
        return tl.get();
    }

    public static void removeDriver(){
        tl.remove();
    }
}