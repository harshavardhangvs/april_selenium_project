package driver;

import enums.ConfigEnums;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import utils.ConfigUtil;
import utils.SystemPropertyUtil;

import java.util.Objects;

public final class DriverFactory {
    private static WebDriver driver;

    private DriverFactory() {
    }

    public static void initDriver() {
        if (Objects.isNull(DriverFactory.getDriver())) {
            //getting browser value as a parameter from maven command(-Dbrowser="chrome")
            String browser = SystemPropertyUtil.getBrowserName();
            if (Objects.isNull(browser) ||
                    isChrome(browser)) {
                setUpChrome();
            } else if (isFireFox(browser)) {
                setUpFirefox();
            } else {
                setUpChrome();
            }
            DriverManager.setDriver(driver);
            DriverFactory.getDriver().get(ConfigUtil.getPropertyValue(ConfigEnums.URL));
        }
    }

    public static void quitDriver() {
        if (!Objects.isNull(DriverFactory.getDriver())) {
            DriverFactory.getDriver().quit();
            DriverManager.removeDriver();
            setDriverToNull();
        }
    }

    public static WebDriver getDriver() {
        return DriverManager.getDriver();
    }

    public static void setDriverToNull() {
        driver = null;
    }

    private static void setUpChrome() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    private static void setUpFirefox() {
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();
    }

    private static boolean isChrome(String browser) {
        return browser.equalsIgnoreCase(ConfigEnums.CHROME.toString().toLowerCase());
    }

    private static boolean isFireFox(String browser) {
        return browser.equalsIgnoreCase(ConfigEnums.FIREFOX.toString().toLowerCase());
    }
}