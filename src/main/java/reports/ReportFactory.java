package reports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import java.util.Objects;

public final class ReportFactory {
    private ReportFactory() {
    }

    private static ExtentReports extent;
    private static ExtentTest test;

    public static void createExtentReport(String suiteName) {
        if (Objects.isNull(extent)) {
            extent = new ExtentReports();
            ExtentSparkReporter spark = new ExtentSparkReporter("target/Spark_" + System.currentTimeMillis() + ".html");
            spark.config().setTheme(Theme.DARK);
            spark.config().setDocumentTitle(suiteName);
            spark.config().setReportName(suiteName);
            extent.attachReporter(spark);
        }
    }

    public static void createExtentTest(String testName, String[] category, String[] authors, String[] devices) {
//        , String authorName, String[] category, String device
        test = extent.createTest(testName);
        for (String group : category) {
            test.assignCategory(group);
        }
        for (String author : authors) {
            test.assignAuthor(author);
        }
        for (String device : devices) {
            test.assignDevice(device);
        }
        ReportManager.setExtentTest(test);
    }

    public static ExtentTest getExtentTest() {
        return ReportManager.getExtentTest();
    }

    public static void flushExtentReport() {
        extent.flush();
    }

}
