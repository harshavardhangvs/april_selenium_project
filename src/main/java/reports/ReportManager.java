package reports;

import com.aventstack.extentreports.ExtentTest;

public final class ReportManager {

    private ReportManager() {
    }

    private static ThreadLocal<ExtentTest> tl = new ThreadLocal<>();

    public static void setExtentTest(ExtentTest extentTest) {
        tl.set(extentTest);
    }

    public static ExtentTest getExtentTest() {
        return tl.get();
    }

    public static void removeExtentTest(){
        tl.remove();
    }
}