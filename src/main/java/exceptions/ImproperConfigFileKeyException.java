package exceptions;

public class ImproperConfigFileKeyException extends RuntimeException {
    public ImproperConfigFileKeyException(String errorMessage) {
        super(errorMessage);
    }
}