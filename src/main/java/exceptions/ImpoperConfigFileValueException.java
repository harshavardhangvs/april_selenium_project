package exceptions;

public class ImpoperConfigFileValueException extends RuntimeException{

    public ImpoperConfigFileValueException(String errorMessage){
        super(errorMessage);
    }
}
