package FrameworkConstants;

public final class Constants {

    private Constants() {
    }

    private final static String ConfigFilePath = System.getProperty("user.dir") + "/src/test/resources/config/Config.properties";
    private final static String ExcelFilePath = System.getProperty("user.dir") + "/src/test/resources/excelSheets/testdata.xlsx";


    public static String getConfigFilePath() {
        return ConfigFilePath;
    }

    public static String getExcelFilePath() {
        return ExcelFilePath;
    }
}